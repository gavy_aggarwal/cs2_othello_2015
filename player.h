#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int minimax(Board *b, Side s, int depth);
    Side otherSide(Side s);
    std::vector<Move> PossibleMoves(Board *b, Side s);
    Move *BestMove(int depth);
    int alphabeta(Board *bd, int depth, int a, int b, Side s);
    
    Side my_side;
    Side other_side;
	Board *board;
	int weights[64];
	
    // For testing
    void setBoard(Board *b);
    bool testingMinimax;
};

#endif
