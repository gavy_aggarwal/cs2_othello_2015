// Written by Gavy Aggarwal

#include "player.h"
#include <stdio.h>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

	board = new Board();
	my_side = side;
	
	int temp[64] = {
			 7,-2, 3, 3, 3, 3,-2, 7,
			-2,-5, 1, 1, 1, 1,-5,-2,
			 3, 1, 1, 1, 1, 1, 1, 3,
			 3, 1, 1, 1, 1, 1, 1, 3,
			 3, 1, 1, 1, 1, 1, 1, 3,
			 3, 1, 1, 1, 1, 1, 1, 3,
			-2,-5, 1, 1, 1, 1,-5,-2,
			 7,-2, 3, 3, 3, 3,-2, 7
		};
	for (int i = 0; i < 64; i++) {
		weights[i] = temp[i];
	}
	
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	delete board;
}


void Player::setBoard(Board *b) {
	delete board;
	board = b;
}

Side Player::otherSide(Side s) {
	if (s == BLACK) {
		return WHITE;
	} else {
		return BLACK;
	}
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    board->doMove(opponentsMove, otherSide(my_side));
    
    Move *move = NULL;
    if (testingMinimax) {
		move = BestMove(2);
	} else {
		move = BestMove(7);
	}
    
	board->doMove(move, my_side);
	return move;
}

std::vector<Move> Player::PossibleMoves(Board *b, Side s) {
	vector<Move> moves;
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			Move *m = new Move(i, j);
			if (b->checkMove(m, s)) {
				moves.push_back(Move(i, j));
			}
			delete m;
		}
	}
	return moves;
}

Move *Player::BestMove(int depth) {
	int j = -9999;
	Move *best = NULL;
	vector<Move> moves = PossibleMoves(board, my_side);
	for (unsigned int i = 0; i < moves.size(); i++) {
		Move m = moves[i];
		Board *b = board->copy();
		b->doMove(&m, my_side);
		//int a = -minimax(b, otherSide(my_side), depth - 1);
		int a = -alphabeta(b, depth - 1, -9999, 9999, otherSide(my_side));
		delete b;
		if (a > j) {
			j = a;
			if (best) {
				delete best;
			}
			best = new Move(m.getX(), m.getY());
		}
	}
	return best;
}

int Player::minimax(Board *b, Side s, int depth) {
	if (depth == 0) {
		return b->count(s) - b->count(otherSide(s));
	}
	std::vector<Move> moves = PossibleMoves(b, s);
	if (moves.size() == 0) {
		return b->count(s) - b->count(otherSide(s));
	}
	int a = -9999;
	for (unsigned int i = 0; i < moves.size(); i++) {
		Move m = moves[i];
		Board *tempBoard = b->copy();
		tempBoard->doMove(&m, s);
		a = max(a, -minimax(tempBoard, otherSide(s), depth - 1));
		delete tempBoard;
	}
	return a;
}

int Player::alphabeta(Board *bd, int depth, int a, int b, Side s) {
	if (depth == 0) {
	 return bd->count(s) - bd->count(otherSide(s));
	}
	std::vector<Move> moves = PossibleMoves(bd, s);
	if (moves.size() == 0) {
		return bd->count(s) - bd->count(otherSide(s));
	}
    if (s == my_side) {
		int v = -9999;
		for (unsigned int i = 0; i < moves.size(); i++) {
			Move m = moves[i];
			Board *tempBoard = bd->copy();
			tempBoard->doMove(&m, s);
			v = max(v, alphabeta(tempBoard, depth - 1, a, b, otherSide(s)));
			a = max(a, v);
			delete tempBoard;
			if (b < a) {
				break;
			}
		}
		return v;
    } else {
		int v = 9999;
		for (unsigned int i = 0; i < moves.size(); i++) {
			Move m = moves[i];
			Board *tempBoard = bd->copy();
			tempBoard->doMove(&m, s);
			v = min(v, alphabeta(tempBoard, depth - 1, a, b, otherSide(s)));
			b = min(b, v);
			delete tempBoard;
			if (b < a) {
				break;
			}
		}
		return v;
	}
}
